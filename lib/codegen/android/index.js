'use strict'
/**
 * Created by safi on 04/03/16 12:19 PM.
 */
var fs = require('fs')
var path = require('path')
var _ = require('underscore')
var cc = require('change-case')
var util = require('./../util')
var android = {}


android.createModel = (mainModel, prInfo)=> {
    console.log(JSON.stringify(mainModel))
    var template = '\
package <%this.pkgName%>.dto;\\n\\n\
import com.fasterxml.jackson.annotation.JsonProperty;\\n\
import java.util.ArrayList;\\n\
import java.util.List;\\n\\n\
public class <% this.fName %>  {\\n\
  <% for(var m in this.model){%>\\n\
@JsonProperty("<%m%>")\\n\
<% if(this.model[m].type=="array"){%>\
private List<<%this.model[m].items.$ref%>> <%m%> = new ArrayList<>();\\n\
<%}else if(this.model[m].type=="number"){%>\
private Double <%m%>;\\n\
<%}else if(this.model[m].type=="integer"){%>\
private Integer <%m%>;\\n\
<%}else if(this.model[m].type=="string"){%>\
private String <%m%>;\\n\
<%}else if(this.model[m].type=="boolean"){%>\
private Boolean <%m%>;\\n\
<%}else if(this.model[m].type=="date"){%>\
private String <%m%>;\\n\
<%}else{%>\
private <%this.model[m].type%> <%m%>;\\n\
<%}%>\
<%}%>\\n\
 <% for(var m in this.model){%>\\n\
<% if(this.model[m].type=="array"){%>\
/**\\n\
*\\n\
*@return\\n\
*The <%m%>\\n\
*/\\n\
@JsonProperty("<%m%>")\\n\
public List<<%this.cc.ucFirst(this.model[m].items.$ref)%>> get<%this.cc.ucFirst(m)%>(){\\n\
    return <%m%>;\\n\
}\\n\
/**\\n\
*\\n\
*@param\\n\
*The <%m%>\\n\
*/\\n\
@JsonProperty("<%m%>")\\n\
public void set<%this.cc.ucFirst(m)%>(List<<%this.cc.ucFirst(this.model[m].items.$ref)%>>  <%m%>){\\n\
   this.<%m%> = <%m%>;\\n\
}\\n\
<%}else if(this.model[m].type=="number"){%>\
/**\\n\
*\\n\
*@return\\n\
*The <%m%>\\n\
*/\\n\
@JsonProperty("<%m%>")\\n\
public Double get<%this.cc.ucFirst(m)%>(){\\n\
    return <%m%>;\\n\
}\\n\
/**\\n\
*\\n\
*@param\\n\
*The <%m%>\\n\
*/\\n\
@JsonProperty("<%m%>")\\n\
public void set<%this.cc.ucFirst(m)%>(Double <%m%>){\\n\
     this.<%m%> = <%m%>;\\n\
}\\n\
<%}else if(this.model[m].type=="integer"){%>\
/**\\n\
*\\n\
*@return\\n\
*The <%m%>\\n\
*/\\n\
@JsonProperty("<%m%>")\\n\
public Integer get<%this.cc.ucFirst(m)%>(){\\n\
    return <%m%>;\\n\
}\\n\
/**\\n\
*\\n\
*@param\\n\
*The <%m%>\\n\
*/\\n\
@JsonProperty("<%m%>")\\n\
public void set<%this.cc.ucFirst(m)%>(Integer <%m%>){\\n\
    this.<%m%> = <%m%>;\\n\
}\\n\
<%}else if(this.model[m].type=="string"){%>\
/**\\n\
*\\n\
*@return\\n\
*The <%m%>\\n\
*/\\n\
@JsonProperty("<%m%>")\\n\
public String get<%this.cc.ucFirst(m)%>(){\\n\
    return <%m%>;\\n\
}\\n\
/**\\n\
*\\n\
*@param\\n\
*The <%m%>\\n\
*/\\n\
@JsonProperty("<%m%>")\\n\
public void set<%this.cc.ucFirst(m)%>(String <%m%>){\\n\
     this.<%m%> = <%m%>;\\n\
}\\n\
<%}else if(this.model[m].type=="boolean"){%>\
/**\\n\
*\\n\
*@return\\n\
*The <%m%>\\n\
*/\\n\
@JsonProperty("<%m%>")\\n\
public Boolean get<%this.cc.ucFirst(m)%>(){\\n\
    return <%m%>;\\n\
}\\n\
/**\\n\
*\\n\
*@param\\n\
*The <%m%>\\n\
*/\\n\
@JsonProperty("<%m%>")\\n\
public void set<%this.cc.ucFirst(m)%>(Boolean <%m%>){\\n\
    this.<%m%> = <%m%>;\\n\
}\\n\
<%}else if(this.model[m].type=="date"){%>\
/**\\n\
*\\n\
*@return\\n\
*The <%m%>\\n\
*/\\n\
@JsonProperty("<%m%>")\\n\
public String get<%this.cc.ucFirst(m)%>(){\\n\
    return <%m%>;\\n\
}\\n\
/**\\n\
*\\n\
*@param\\n\
*The <%m%>\\n\
*/\\n\
@JsonProperty("<%m%>")\\n\
public void set<%this.cc.ucFirst(m)%>(String <%m%>){\\n\
    this.<%m%> = <%m%>;\\n\
}\\n\
<%}else{%>\
/**\\n\
*\\n\
*@return\\n\
*The <%m%>\\n\
*/\\n\
@JsonProperty("<%m%>")\\n\
public <%this.cc.ucFirst(m)%> get<%this.cc.ucFirst(m)%>(){\\n\
    return <%m%>;\\n\
}\\n\
/**\\n\
*\\n\
*@param\\n\
*The <%m%>\\n\
*/\\n\
@JsonProperty("<%m%>")\\n\
public void set<%this.cc.ucFirst(m)%>(<%this.cc.ucFirst(m)%> <%m%>){\\n\
    this.<%m%> = <%m%>;\\n\
}\\n\
<%}%>\
<%}%>\\n\
\
}';
    var basePath = path.join(prInfo.tmpBasePath, "dto");
    // console.log(JSON.stringify(mainModel))
    var pkgName = "com.liad.accounting"
    var modelTxt = "", i = 0, clName = ""
    _.each(mainModel, function (model) {
            _.each(model, function (classObj, fName) {
                clName = cc.ucFirst(fName)
                _.each(classObj, function (obj) {

                    if (obj.type == "array") {
                        if (!obj.items) {
                            obj.items = {
                                "$ref": clName
                            }
                        } else if (obj.items.type) {
                            obj.items.$ref = obj.items.type
                        }
                    }

                    if (obj.items && obj.items.$ref) {
                        obj.items.$ref = cc.ucFirst(obj.items.$ref.split('-')[0]);
                    }

                })
                modelTxt = util.templateEngine(template, {cc: cc, pkgName: pkgName, fName: clName, model: classObj});
                //  if (clName == "Userlist") {
                // console.log(modelTxt)
                fs.writeFileSync(path.join(basePath, clName) + ".java", modelTxt, {})
                //   i++;
                // }

            });
        }
    )

}

module.exports = android