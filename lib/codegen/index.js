'use strict'
/**
 * Created by safi on 29/01/16 1:25 PM.
 */
const shortId = require('shortid')
const fs = require('fs')
const path = require('path')
var archiver = require('archiver')
const cc = require('change-case')
const swiftMaker = require('./swift/index')
const androidMaker = require('./android/index')
var mainHandler = {};
var external = {};

const getFinalMatchedData = (swgData) => {

    var mainHandler = {};
    var mainModel = {}, basePath;
    const getHandlerName = (desc)=> {
        let spls = desc.split(" ")
        let r = "";
        for (let s in spls)
            r += cc.ucFirst(spls[s])
        r = r.replace(/[^a-zA-Z ]/g, "")
        return cc.lcFirst(r);
    }
    const checkUniqueHandler = (hdlerName, key)=> {
        for (let k in mainHandler[hdlerName]) {
            if (k == key)
                return false;
        }
        return true;
    }
    const checkUniqueModel = (key)=> {
        key = key.split("-")[0]
        for (let h in mainModel) {
            for (let m in mainModel[h]) {
                if (m == key)
                    return h;
            }
        }
        return null;
    }
    const parseModelProperty = (model, ah, mName)=> {
        let pMdl = {};
        if (ah) {
            pMdl = mainModel [ah][mName]
        }
        for (let p in model.properties) {

            if (pMdl && !pMdl[p]) {

                model.properties[p].type = model.properties[p].type.split("-")[0]
                pMdl[p] = model.properties[p]
            }

        }
        return pMdl;
    }
    const parseModel = (data)=> {
        for (let h in data) {
            basePath = data[h].basePath;
            for (let m in data[h].models) {
                let key = m.split("-")[0]
                let aH = checkUniqueModel(m)
                if (aH) {
                    mainModel[aH][key] = parseModelProperty(data[h].models[m], aH, key)
                } else {
                    mainModel[h] = mainModel[h] ? mainModel[h] : {}
                    mainModel[h][key] = parseModelProperty(data[h].models[m])
                }
            }
        }

        //Reformat to new method

    }
    //Param meter Parse
    const getParamers = (handlerName, params)=> {
        var pParams = [];
        if (params.length > 4) {

        } else { //No Need customization in request

        }
        return params;
    }
    const parseHandler = (data)=> {
        for (let k in data) {
            for (let a in data[k].apis) {
                var tHand = getHandlerName(data[k].apis[a].description)
                if (!mainHandler[k] || checkUniqueHandler(k, tHand)) {

                    mainHandler[k] = mainHandler[k] ? mainHandler[k] : {};
                    let apiObj = data[k].apis[a].operations[0]
                    mainHandler[k][tHand] = {
                        method: apiObj.method,
                        type  : apiObj.type,
                        params: getParamers(k, apiObj.parameters),
                        path  : data[k].apis[a].path
                    };
                }
            }

        }
    }
    parseModel(swgData);
    parseHandler(swgData);

    return {
        m: mainModel,
        h: mainHandler
    }
};


const makeZip = (path, dest, callback)=> {
    var output = fs.createWriteStream(dest);
    var archive = archiver('zip');

    output.on('close', function () {
        // console.log(archive.pointer() + ' total bytes');
        //console.log('archiver has been finalized and the output file descriptor has closed.');
        callback(null, true)
    });

    archive.on('error', function (err) {
        throw err;
    });
    archive.pipe(output);
    archive.bulk([
        {expand: true, cwd: path, src: ['**'], dest: 'source'}
    ]);
    archive.finalize();
}

const createIosSwift = (mainObj, prInfo)=> {

    let getHandlers = swiftMaker.createHandler(mainHandler);
    let getConstants = swiftMaker.createConstants(mainHandler, basePath, "./swift/lib/");
    let getModels = swiftMaker.createModel(mainModel, basePath);
}

const createAndJava = (mainObj, prInfo)=> {
    // fs.mkdirSync(basePath)
    //let getHandlers = androidMaker.createHandler(mainObj.h, prInfo);
    //let getConstants = androidMaker.createConstants(mainObj.h, prInfo);
    fs.mkdirSync(path.join(prInfo.tmpBasePath, "dto"))
    androidMaker.createModel(mainObj.m, prInfo);

}


external.generateCodeGen = (mainData, prInfo, callBack)=> {
    let tempId = shortId.generate();
    let basePath = path.join("./node_modules/hm-hapi-swagger/public/swaggerui/temp/", tempId)
    fs.mkdirSync(basePath)
    prInfo.tmpBasePath = basePath
    var mdlHandlerObj = getFinalMatchedData(mainData);
    if (prInfo.pl == "IOS") {
        createIosSwift(mdlHandlerObj, prInfo)
    } else {
        createAndJava(mdlHandlerObj, prInfo)
    }
    //make Zip
    makeZip(prInfo.tmpBasePath, basePath + ".zip", function (er, res) {
        callBack(null, {
            url: "/docs/swaggerui/temp/" + tempId + ".zip"
        })
    })
}

module.exports = external;