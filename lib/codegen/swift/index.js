'use strict'
/**
 * Created by safi on 29/01/16 10:27 PM.
 */
var fs = require('fs')
var cc = require('change-case')
var util = require('./../util')
var swift = {}

swift.createHandler = (mainHandler)=> {
    //  console.log(mainHandler);
    var template = '//\\n\
//  <%this.fName%>\\n\
//  Created by hm-code-gen on <% new Date %>\\n\
//  Copyright (c) 2015 hakuna. All rights reserved.\\n\
//\\n\
\\n\
    import UIKit\\n\
\\n\
    class <%this.cName%>: NSObject {\\n\
    \\n\
    <% for(var h in this.handler){%>\\n\
    func <%h%>(<%for(var pIn in this.handler[h].params){%><%this.handler[h].params[pIn].name%> : <%this.handler[h].params[pIn].type%>,<%}%>Completion Handler : (<% this.handler[h].type %>!, NSError?) -> Void)\\n\
    {\\n\
        let url : NSString = NSString(format:APIConstants.getUrl(APIConstants.<%h%>)<%this.handler[h].ulParam%>)\\n\
        let headers  :  Dictionary <String,String> = <%this.handler[h].hValue%>\\n\
       \
        WebServiceHandler().<%this.handler[h].method.toLowerCase()%>Method(url as String,  headers:headers, body: <%this.handler[h].bValue%>) { (responseDictionar, error) -> Void in\\n\
        if responseDictionar != nil{\\n\
            Handler(<% this.handler[h].type%>().initWithJsonRootDictionary(responseDictionar),nil)\\n\
           } else {\\n\
                Handler(nil,error)\\n\
           }\\n\
           return Void()\\n\
        }\\n\
    }\\n\
   <%}%>\
    }\\n\
';
    const format = function (str) {
        var temp = "";
        var reg = /{([^}]+)?}/g, match, t;
        while (match = reg.exec(str)) {
            // console.log(match)
            t = str.slice(0, match.index);
            str = t + "%@" + str.substr(match.index + match[0].length)
            temp += ',' + match[1]
        }
        return temp;
    }
    const getHValue = function (params) {
        let hValue = ''
        for (let i in params) {
            if (params[i].paramType == "header") {
                hValue += ',"' + params[i].name + '":' + params[i].name
            }
        }
        if (hValue == '')
            hValue = " Dictionary()"
        else
            hValue = '[' + hValue.substr(1) + ']';
        return hValue
    }
    for (var h in mainHandler) {
        var cName = cc.ucFirst(h) + 'DataHandler'
        var fName = cName + '.swift'
        for (var m in mainHandler[h]) {
            mainHandler[h][m].ulParam = format(mainHandler[h][m].path)
            mainHandler[h][m].hValue = getHValue(mainHandler[h][m].params)
            mainHandler[h][m].bValue = "nil";
            for (var pIn in mainHandler[h][m].params) {
                if (mainHandler[h][m].params[pIn].type == "string")
                    mainHandler[h][m].params[pIn].type = "String"
                else if (mainHandler[h][m].params[pIn].type == "body")
                    mainHandler[h][m].bValue = "swiftToJsonParser(body) as? [String : AnyObject]";
            }
        }

        let modelTxt = util.templateEngine(template, {
            fName  : fName,
            cName  : cName,
            handler: mainHandler[h]
        });
        fs.writeFile("./swift/lib/Handlers/" + fName, modelTxt, {}, function (err, res) {
            console.log(err, res)
        })
    }

};


swift.createModel = (mainModel)=> {

    var template = '//\\n\
//  <%this.fName%>\\n\
//  Created by hm-code-gen on <% new Date %>\\n\
//  Copyright (c) 2015 hakuna. All rights reserved.\\n\
//\\n\
\\n\
    import UIKit\\n\
    \
    <%if(this.h==1){%>\\n\
    class BaseDTO : NSObject {\\n\
\\n\
    }\\n\
        <%}%>\
<% for(var m in this.model){%>\\n\
    class <% m %> : BaseDTO {\\n\
   <% for(var pr in this.model[m].model){%>\\n\
        <% if(this.model[m].model[pr].type=="array"){%>\
    var <%pr%><% for(var s=0;s<(this.model[m].s1 - pr.length);s++){ %> <%}%> : NSArray = NSArray()\
    <%}else if(this.model[m].model[pr].type=="number"){%>\
    var <%pr%><% for(var s=0;s<(this.model[m].s1 - pr.length);s++){ %> <%}%> : Double  = 0.0\
     <%}else if(this.model[m].model[pr].type=="integer"){%>\
    var <%pr%><% for(var s=0;s<(this.model[m].s1 - pr.length);s++){ %> <%}%> : Double  = 0.0\
      <%}else if(this.model[m].model[pr].type=="string"){%>\
    var <%pr%><% for(var s=0;s<(this.model[m].s1 - pr.length);s++){ %> <%}%> : String  = ""\
       <%}else if(this.model[m].model[pr].type=="boolean"){%>\
    var <%pr%><% for(var s=0;s<(this.model[m].s1 - pr.length);s++){ %> <%}%> : Bool    = false\
     <%}else if(this.model[m].model[pr].type=="date"){%>\
    var <%pr%><% for(var s=0;s<(this.model[m].s1 - pr.length);s++){ %> <%}%> : String    = ""\
      <%}else{%>\
    var <%pr%><% for(var s=0;s<(this.model[m].s1 - pr.length);s++){ %> <%}%> : <%this.model[m].model[pr].type%><%if(this.model[m].model[pr].required){%>!<%}else{%>?<%}%>\
      <%}%>\
<%}%>\\n\
    }\\n\
<%}%>';
    var hc = 0;
    const format = function (str) {
        var temp = "";
        var reg = /{([^}]+)?}/g, match, t;
        while (match = reg.exec(str)) {
            console.log(match)
            t = str.slice(0, match.index);
            str = t + "%@" + str.substr(match.index + match[0].length)
        }
        return str;
    }
    for (var h in mainModel) {
        hc++;
        let modelTxt = "", fName = "API" + cc.ucFirst(h) + "DTO.swift";
        let enums = {}, s1 = 0, s2 = 0
        for (let m in mainModel[h]) {
            var c = Object.keys(mainModel[h][m]);
            if (c.length > 0) {
                for (let pr in mainModel[h][m]) {
                    if (s1 < pr.length)
                        s1 = pr.length
                    if (s2 < mainModel[h][m][pr].type.length)
                        s2 = mainModel[h][m][pr].type.length
                }
                mainModel[h][m] = {
                    s1   : s1,
                    s2   : s2,
                    model: mainModel[h][m]
                }
            } else {
                delete mainModel[h][m]
            }
        }

        modelTxt = util.templateEngine(template, {cc: cc, fName: fName, h: hc, s1: 0, s2: 0, model: mainModel[h]});
        fs.writeFile("./swift/lib/Data/" + fName, modelTxt, {}, function (err, res) {
            console.log(err, res)
        })
    }
};

swift.createConstants = (mainHandler, basePath, filePath)=> {

    var template = '//\\n\
//  APIConstants.swift\\n\
//  Created by hm-code-gen on <% new Date %>\\n\
//  Copyright (c) 2015 hakuna. All rights reserved.\\n\
//\\n\
\\n\
    import UIKit\\n\
\\n\
    class APIConstants: NSObject {\\n\
\\n\
   \
 <%for(var h in this.handler) {%>\\n\
         //<% h %>\\n\
         <%for(var p in this.handler[h]) {%>\\n\
             static let  <%p%><% for(var s=0;s<(this.space - p.length);s++){ %> <%}%>  = "<%this.handler[h][p].path%>"\
   <%}%>\\n\
  \\n\
   <%}%>\\n\
        }\\n\
        \\n\
  // Endpoint \\n\
    static let Base_url = "<% this.basePath %>" \\n\
 \\n\
    \\n\
     class func getUrl(urlString : String) -> String{\\n\
        return "\(Base_url)\(urlString)"\\n\
    }\
\
\\n\
    \\n\
    \\n\
}';
    var maxStr = 0
    const formatPath = (str)=> {
        var reg = /{([^}]+)?}/g, match, t;
        while (match = reg.exec(str)) {
            t = str.slice(0, match.index);
            str = t + "%@" + str.substr(match.index + match[0].length)
        }
        return str;
    }
    for (let h in mainHandler) {
        for (let p in mainHandler[h]) {
            if (maxStr < p.length)
                maxStr = p.length
            mainHandler[h][p].path = formatPath(mainHandler[h][p].path)
        }
    }
    var text = util.templateEngine(template, {basePath: basePath, handler: mainHandler, space: maxStr});
    fs.writeFile(filePath + "/Constants/APIConstants.swift", text, {}, function (err, res) {
        console.log(err, res)
    })


}

module.exports = swift;




